﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchScript;
using TouchScript.Gestures;
using TouchScript.Gestures.TransformGestures;
using DG.Tweening;

public class handleRotationalTimeControl : MonoBehaviour
{

    bool isPressed = false;
    public GameObject visibleHandle;
    public float angle;
    public GameObject visibleHandleParent;
    public Light mainLight;
    public GameObject mainLightParent;
    float lightIntensity;
    //public GameObject blackSkyOverlay;
    public Color sunOrange;
    public Color moonNight;
    handleState HandleState;
    public AudioSource audioLoopCrickets;
    public AudioSource audioLoopBirds;


    // Start is called before the first frame update
    void Start()
    {

        HandleState = GameObject.Find("GameManager").GetComponent<handleState>();
        GetComponent<ReleaseGesture>().StateChanged += releaseHandler;
        GetComponent<PressGesture>().StateChanged += pressHandler;

    }

    // Update is called once per frame
    void Update()
    {
        if (isPressed == true)
        {
            rotate();
        }

        setDayNightBackgroundLoops();
    }

    void setDayNightBackgroundLoops()
    {

        if (HandleState.soundScapeIsInDrawer == false)
        {
            if (angle < -90 || angle > 90)
            {

                audioLoopBirds.DOFade(0, 2);
                audioLoopCrickets.DOFade(0.5f, 2);
            }
            else
            {

                audioLoopBirds.DOFade(0.5f, 2);
                audioLoopCrickets.DOFade(0, 2);
            }
        }
        else
        {
            audioLoopBirds.DOFade(0, 1);
            audioLoopCrickets.DOFade(0, 1);
        }

        

    }

    private void releaseHandler(object sender, GestureStateChangeEventArgs e)
    {

        if (e.State == Gesture.GestureState.Ended)
        {
            //print("released!");

            isPressed = false;


            // handlePaddleTouch();

            resetHiddenDraggablePos();



        }

    }

    private void pressHandler(object sender, GestureStateChangeEventArgs e)
    {

        if (e.State == Gesture.GestureState.Ended)
        {
            //print("pressed!");

            isPressed = true;

            

        }

    }

    void resetHiddenDraggablePos()
    {
        transform.position = visibleHandle.transform.position;
    }

    

    void rotate()
    {

        

        Vector3 vectorToTarget = this.transform.position - visibleHandleParent.transform.position;

        Debug.DrawLine(this.transform.position, visibleHandleParent.transform.position, Color.green, Time.deltaTime, false);
        angle = (Mathf.Atan2(vectorToTarget.x, vectorToTarget.y) * Mathf.Rad2Deg);

        //visibleHandleParent.transform.eulerAngles = new Vector3(40, 0, ClampAngle(-angle, -45, 45));
        visibleHandleParent.transform.eulerAngles = new Vector3(40, 0, -angle);
        visibleHandle.transform.eulerAngles = new Vector3(40, 0, angle*2);

        //mainLightParent.transform.eulerAngles = new Vector3(0, ClampAngle(-angle, -45, 45), 0);
        mainLightParent.transform.eulerAngles = new Vector3(0, -angle, 0);

        if (visibleHandleParent.transform.localRotation.z >= 0)
        {

            lightIntensity = 1-((visibleHandleParent.transform.eulerAngles.z / 90)*0.8f);
            //
            
        }
        else
        {
            lightIntensity = 1 - ((((visibleHandleParent.transform.eulerAngles.z - 360) / 90) * 0.8f) * -1);            
        }

        mainLight.intensity = lightIntensity/2 + 0.5f;
        mainLight.shadowStrength = lightIntensity / 2 + 0.5f;

       // blackSkyOverlay.GetComponent<SpriteRenderer>().DOFade(1 - lightIntensity, 1);

        //print(angle);

        //if (angle < -90 || angle > 90)
        //{
        //    visibleHandle.GetComponent<SpriteRenderer>().DOColor(moonNight, 2);
           
        //}
        //else
        //{
        //    visibleHandle.GetComponent<SpriteRenderer>().DOColor(sunOrange, 2);
           
        //}

        

    }

    //praise baby jebus for someone solving rotation probs outside 0-360 range
    //https://answers.unity.com/questions/141775/limit-local-rotation.html

    protected float ClampAngle(float angle, float min, float max)
    {

        angle = NormalizeAngle(angle);
        if (angle > 180)
        {
            angle -= 360;
        }
        else if (angle < -180)
        {
            angle += 360;
        }

        min = NormalizeAngle(min);
        if (min > 180)
        {
            min -= 360;
        }
        else if (min < -180)
        {
            min += 360;
        }

        max = NormalizeAngle(max);
        if (max > 180)
        {
            max -= 360;
        }
        else if (max < -180)
        {
            max += 360;
        }

        // Aim is, convert angles to -180 until 180.
        return Mathf.Clamp(angle, min, max);
    }

    /** If angles over 360 or under 360 degree, then normalize them.
     */
    protected float NormalizeAngle(float angle)
    {
        while (angle > 360)
            angle -= 360;
        while (angle < 0)
            angle += 360;
        return angle;
    }
}
