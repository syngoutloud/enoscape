﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class playSoundOnCollision : MonoBehaviour
{

    handleState HandleState;
    //public GameObject speechBubble;
    Vector3 speechBubbleStartingScale;
    public AudioClip[] audioSources;
    AudioSource audioSource;
    public GameObject attachedSphere;
    Vector3 sphereScale;
    float scaleFactor;
    public int decayLength;

    // Start is called before the first frame update
    void Start()
    {
        scaleFactor = 0.15f;
        sphereScale = attachedSphere.transform.localScale;
        HandleState = GameObject.Find("GameManager").GetComponent<handleState>();
        
        audioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        //print("hit!" + gameObject.name);
        if (GetComponent<AudioSource>().isPlaying == false && HandleState.soundScapeIsInDrawer == false)
        {
            audioSource.clip = audioSources[Random.Range(0, audioSources.Length)];
            //audioSource.clip = audioSources[1];
            audioSource.PlayOneShot(audioSource.clip, 1);
            //speechBubble.transform.DOScale(speechBubbleStartingScale, 0.3f).SetEase(Ease.OutBack).OnComplete(resetSpeechBubble);
            attachedSphere.transform.DOPunchScale(new Vector3(sphereScale.x* scaleFactor, sphereScale.y * scaleFactor, sphereScale.z * scaleFactor), decayLength, 7, 1);
        }


        //goInOppositeDirection();
    }

    

    
}
