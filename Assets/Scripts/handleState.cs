﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TouchScript.Gestures.TransformGestures;

public class handleState : MonoBehaviour
{

    public bool soundScapeIsInDrawer = true;
    public GameObject targetSpace;
    public GameObject targetDrawer;
    public GameObject jungleSpace;
    public GameObject closeX;
    public SphereCollider jungleSpaceSphereCollider;
    public TransformGesture jungleSpaceTransformGesture;
    public AudioSource jungleSpaceBackgroundAudio;
    public AudioSource raincloudAudio;
    public GameObject sphereBackgroundColor;
    public Color skyColorDay;
    public Color skyColorNight;
    public Color skyColorOff;
    public Color cameraBkgColorOff;
    public Color cameraBkgColorDay;
    public GameObject drawerBackgroundCircle;
    public GameObject rainCloud;
    //public GameObject rainParticles;
    Vector3 rainCloudStartingPos;
    public GameObject timeControl;
    //public AudioSource audioLoopCrickets;
    //public AudioSource audioLoopBirds;
    //public AudioSource audioLoopBatFlapping;
    public GameObject skyOverlay;
    public GameObject timeControlRotator;
    public GameObject timeControlHiddenDragHandle;
    public GameObject timeControlVisibleDragHandle;
    public Light mainLight;
    public GameObject mainLightParent;
    public bool viewIsIso = true;
    public GameObject camParent;
    public GameObject iconIso;
    public GameObject iconTopDown;





    // Start is called before the first frame update
    void Start()
    {
        Application.targetFrameRate = 60;
        soundScapeIsInDrawer = true;
        rainCloudStartingPos = rainCloud.transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setCamView()
    {
        if (viewIsIso == true)
        {
            //camParent.transform.eulerAngles = new Vector3(0, 0, 0);
            camParent.transform.DORotate(new Vector3(0, 0, 0), 0.4f);
        }
        else
        {
            //camParent.transform.eulerAngles = new Vector3(50, 0, 0);
            camParent.transform.DORotate(new Vector3(50, 0, 0), 0.4f);
        }
    }

    public void returnJungleToDrawer()
    {
        jungleSpace.transform.DOMove(targetDrawer.transform.position, 0.4f).SetEase(Ease.OutBack);
        jungleSpace.transform.DOScale(new Vector3(0.2f, 0.2f, 0.2f), 0.4f).SetEase(Ease.OutBack);
        jungleSpace.GetComponent<SphereCollider>().enabled = true;
        jungleSpace.GetComponent<TransformGesture>().enabled = true;
        closeX.SetActive(false);
        soundScapeIsInDrawer = true;
        setJungleDraggableState();
        skyOverlay.GetComponent<SpriteRenderer>().DOFade(0, 1);

        timeControlRotator.transform.eulerAngles = new Vector3(40, 0, -0);
        timeControlHiddenDragHandle.transform.position = timeControlVisibleDragHandle.transform.position;
        
        mainLightParent.transform.eulerAngles = new Vector3(0, 0, 0);

        viewIsIso = true;
        setCamView();
        GameObject.Find("iconTopDownHighlight").GetComponent<SpriteRenderer>().DOFade(0, 0.2f);
        GameObject.Find("iconIsoHighlight").GetComponent<SpriteRenderer>().DOFade(1, 0.2f);

        iconIso.SetActive(false);
        iconTopDown.SetActive(false);

        


    }

    public void setJungleDraggableState()
    {
        if (soundScapeIsInDrawer == true)
        {
            jungleSpaceSphereCollider.enabled = true;
            jungleSpaceTransformGesture.enabled = true;
            closeX.SetActive(false);
           // sphereBackgroundColor.GetComponent<SpriteRenderer>().DOColor(skyColorOff, 0.5f);
            //Camera.main.DOColor(cameraBkgColorOff, 0.5f);
            //jungleSpaceBackgroundAudio.DOFade(0, 1);
            //audioLoopBirds.DOFade(0, 1);
            //audioLoopCrickets.DOFade(0, 1);
            raincloudAudio.DOFade(0f, 1);
            drawerBackgroundCircle.GetComponent<SpriteRenderer>().DOFade(1, 0.5f);
            rainCloud.transform.DOLocalMove(rainCloudStartingPos, 0.5f);
            //rainParticles.SetActive(false);
            timeControl.transform.DOScale(new Vector3(0f, 0f, 0f), 0.25f);
            mainLight.DOIntensity(1, 0.4f);
            //mainLight.DOShadowStrength(1, 0.4f);
            //audioLoopBatFlapping.DOFade(0, 1);
            iconIso.SetActive(false);
            iconTopDown.SetActive(false);

        }
        else
        {
            jungleSpaceSphereCollider.enabled = false;
            jungleSpaceTransformGesture.enabled = false;
            closeX.SetActive(true);
           // Camera.main.DOColor(cameraBkgColorDay, 0.5f);
           // sphereBackgroundColor.GetComponent<SpriteRenderer>().DOColor(skyColorDay, 0.5f);
            //jungleSpaceBackgroundAudio.DOFade(0.165f, 1);
            raincloudAudio.DOFade(1f, 1);
            drawerBackgroundCircle.GetComponent<SpriteRenderer>().DOFade(0, 0.5f);
            //rainParticles.SetActive(true);
            timeControl.transform.DOScale(new Vector3(1, 1, 1), 0.4f).SetEase(Ease.OutBack).SetDelay(0.3f);
            
            //audioLoopBatFlapping.DOFade(0.45f, 1);
            iconIso.SetActive(true);
            iconTopDown.SetActive(true);
        }
    }
}
